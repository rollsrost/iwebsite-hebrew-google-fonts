<?php 
if( ! defined('WP_UNINSTALL_PLUGIN') )
	exit;

delete_option( 'ihgf_options' );
delete_option( 'iwebsite_hebrewfonts_data' );
