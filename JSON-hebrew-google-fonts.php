<?php
$fonts = '{
 "kind": "webfonts#webfontList",
 "items": [
  {
   "kind": "webfonts#webfont",
   "family": "Alef",
   "variants": [
    "regular",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Alef Hebrew",
   "variants": [
    "regular",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  },   
  {
   "kind": "webfonts#webfont",
   "family": "Arimo",
   "variants": [
    "regular",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Assistant",
   "variants": [
    "300",
    "regular",
    "600",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Amatica SC",
   "variants": [
    "regular",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "David Libre",
   "variants": [
    "regular",
    "500",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Miriam Libre",
   "variants": [
    "regular",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Frank Ruhl Libre",
   "variants": [
    "300",
    "regular",
    "500",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Suez One",
   "variants": [
    "regular"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Secular One",
   "variants": [
    "regular"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Heebo",
   "variants": [
    "300",
    "regular",
    "500",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Cousine",
   "variants": [
    "regular",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Varela Round",
   "variants": [
    "regular"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Rubik",
   "variants": [
    "300",
    "regular",
    "500",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Tinos",
   "variants": [
    "regular",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  },
  {
   "kind": "webfonts#webfont",
   "family": "Open Sans Hebrew",
   "variants": [
    "regular",
    "700"
   ],
   "subsets": [
   "hebrew"
   ]
  }  
 ]
}';
?>