=== Hebrew Google Fonts ===
Contributors: iWebsite
Tags: Google fonts, fonts, font, free fonts, Google hebrew fonts, hebrew google font, hebrew google fonts, hebrew fonts, hebrew font, typography, css, design, plugin, WordPress Google Fonts Plugin, Google Webfonts, Google Fonts WordPress, WordPress Fonts, Theme Fonts, Theme Fonts Plugin
Requires at least: 4.6.1
Tested up to: 4.6.1
Stable tag: assets
License: GPLv2 or later

The WP Hebrew Google Fonts plugin allows you to easily add fonts from the Google Font Directory to your Wordpress theme. 
Also you can add custom free hebrew fonts.

== Description ==
The useful and great developments in web typography - is the Hebrew Google's free font plugin. 
Hebrew Fonts in plugin:
1. Alef
2. Alef Hebrew ( old version of Alef )
3. Arimo
4. Assistant
5. Amatica SC
6. David Libre
7. Miriam Libre
8. Frank Ruhl Libre
9. Suez One
10. Secular One
11. Cousine
12. Varela Round
13. Rubik
14. Tinos
15. Open Sans Hebrew

Our plugin is very easy and simple way to add hebrew fonts to any WordPress powered site in hebrew language without coding. 
It even easier to use hebrew free service to add high quality fonts to your Wordpress theme and repair your website design. 
Not only does this plugin add the necessary Google code, 
also this plugin gives you an ability to assign the hebrew fonts to specific CSS selectors of your website in the admin settings area. 
You can target the hebrew fonts also from your own theme's stylesheet. It's compatible with any theme. 

One more option is style your content in wordpress editor with custom hebrew font.
It's easy, just try it.

= What does this plugin do? =

1. Full control of your theme's typography (no coding required).
2. Choose from 14 hebrew google fonts** and font variants to insert into your website without coding.
3. Create your own font controls and rules (no coding required).
4. Change the look of your website with the click of a button.
5. Automatically enqueues all stylesheets for your chosen google fonts.
6. Style your content in wordpress editor with custom hebrew font

= Who is this Plugin ideal for? =

* Anyone who is looking for an easy way to use hebrew google fonts in their theme without coding.
* You can use this plugin to add custom hebrew google webfonts to your theme.
* Great for use on client projects or for use on existing websites.
* People that are happy with their theme but want an easy way to change the typography.
* Anyone with basic knowledge of CSS Selectors (in order to add custom font rules).











