jQuery(document).ready(function($) {
  $('.webfonts-select').each(function(){
    var checked = $(this).find('option:checked').val();
    if ( checked == 'off' ){
      $(this).parent().find('.webfonts-selections').css('display','none');
    }
  });
  $('.webfonts-select').change(function() {

    var ihgf_aj_name = $(this).parent().attr("id");
    var ihgf_aj_family = $(this).val(); 
    var data = {
      action: 'iwebsite_hebrewfont_action',
      googlefont_ajax_family: ihgf_aj_family,
      googlefont_ajax_name: ihgf_aj_name
    };

    if(ihgf_aj_family == '' || ihgf_aj_family == 'off'){
      ihgf_reset_selections(ihgf_aj_name, ihgf_aj_family);
      return false;
    }else{            
      $('#' + ihgf_aj_name + ' .webfonts-waiting').addClass('show');
      $('#' + ihgf_aj_name + ' .webfonts-selections').addClass('hide');
      $('#' + ihgf_aj_name + ' .show-hide').addClass('show');
      $.post(ajaxurl, data, function(response) {
        $('#' + ihgf_aj_name + ' .webfonts-selections').replaceWith(response);
        // ihgf_reset_selections(ihgf_aj_name, ihgf_aj_family);
        $('#' + ihgf_aj_name + ' .webfonts-waiting').removeClass('show');
        $('#' + ihgf_aj_name + ' .show-hide').removeClass('show');
        // $('#' + ihgf_aj_name + ' .webfonts-selections').removeClass('hide');
        // show_selected_items($('#' + ihgf_aj_name));
      });
    }
  });
  
  $('.show-hide').click(function() {
    
    var parent = $(this).parent();
    
    if ($(this).hasClass("show")){
      hide_selected_items(parent);
    }else{
      show_selected_items(parent);
    }
    
    return false;
  });
  
  
  /* update the selections on change */
  function ihgf_reset_selections(fontblock, selected_font) {
    
    var name = get_normalized_name(selected_font);
    var parent = $('#' + fontblock);
    var parentid = $(parent).attr("id");
      
    if(selected_font != '' && selected_font != 'off'){                    
      /* pre select variant and character set */
      pre_select_items(parent, name);
    }else{
      /* clear all the items and hide them */
      $('#' + parentid + ' .webfonts-variants :checked').attr('checked', false);
      $('#' + parentid + ' .webfonts-usage :checked').attr('checked', false);
      $('#' + parentid + ' .webfonts-subsets :checked').attr('checked', false);
      $('#' + parentid + '_css').val(' ');

      hide_selected_items(parent);
    }
  };
  
  function show_selected_items(parent){
    /* limit all our actions to just within this specific selection */
    var parentid = $(parent).attr("id");
    $('#show-hide-' + parentid ).removeClass('hide').addClass('show');
    $('#' + parentid + ' .webfonts-selections').fadeIn(500);
  }
  
  function hide_selected_items(parent){
    /* limit all our actions to just within this specific selection */
    var parentid = $(parent).attr("id");
    $('#show-hide-' + parentid ).removeClass('show').addClass('hide');
    $('#' + parentid + ' .webfonts-selections').fadeOut(500);
  }
  
  function get_normalized_name(name){
    return (name.replace(" ","-"));
  }
  
  function pre_select_items(parent, normalized){  
    var parentid = $(parent).attr('id');
    /* select 'regular' variant if available, or only variant, or first one */
    var variants = $('#' + parentid + ' .variant-' + normalized + ' .check');
    var regular = $('#' + parentid + ' .variant-' + normalized + ' [value="regular"]');
    
    if(variants.size() > 1){
      if(regular.size()==1){
        regular.attr('checked',true);
      }else{
        variants.first().attr('checked',true)
      }
    }
    if(variants.size()==1){
      variants.attr('checked',true);
      variants.attr('readonly','readonly');
    }
    
  }

  $('.special-element-font').on('click', function(){
      var id = $(this).data('font-id');
      var parent = $(this).closest('div');
      $( '#googlefont'+ id ).show();
  });
});