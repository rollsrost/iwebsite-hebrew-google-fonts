<?php
/**
 * Plugin Name: iWebsite Hebrew Google Font
 * Plugin URI: https://iwebsite.co.il/plugins/hebrew-google-fonts/
 * Description: Hebrew Font plugin provides an easy way to use different Hebrew Google fonts on your WordPress website.
 * Version: 1.0.0
 * Author: iWebsite 
 * Author URI: https://iwebsite.co.il
 * 
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

// Globals
define( 'ihgf_PLUGIN_URL', plugins_url() . '/iwebsite-hebrew-google-fonts' );
define( 'ihgf_PLUGIN_VERSION', '1.0.0' );

if ( !class_exists('Iwebsite_hebrew_google_fonts') ) {
  class Iwebsite_hebrew_google_fonts {
    /**
    * @var string The options string name for this plugin
    */
    public $optionsName = 'ihgf_options';

    /**
    * @var string $localizationDomain Domain used for localization
    */
    public $localizationDomain = "iwebsite_hebrew_google_fonts";

    /**
    * @var array $options Stores the options for this plugin
    */
    public $options = array();

    /**
    * @var array $ihgf_notices Stores the notices
    */

    public $ihgf_notices = array();
    
    /**
    * @var string $ihgf_data_option_name name of option in database where store settings of foants
    */

    public $ihgf_data_option_name = "iwebsite_hebrewfonts_data";

    public $ihgf_fonts_file = 'JSON-hebrew-google-fonts.php';

    public $ihgf_filename = 'iwebsite-hebrew-googlefonts.php';

    // public $ihgf_usage_elements = array(
    //   'body'      => $this->body_label, 
    //   'heading1'  => 'Headline 1 (h1 tags)', 
    //   'heading2'  => 'Headline 2 (h2 tags)', 
    //   'heading3'  => 'Headline 3 (h3 tags)', 
    //   'heading4'  => 'Headline 4 (h4 tags)', 
    //   'heading5'  => 'Headline 5 (h5 tags)', 
    //   'heading6'  => 'Headline 6 (h6 tags)', 
    //   'blockquote'=> 'Blockquotes', 
    //   'p'         => 'Paragraphs (p tags)', 
    //   'li'        => 'Lists (li tags)', 
    //   'a'         => 'Links', 
    //   'span'      => 'Span'
    // );
    public $ihgf_usage_elements;

    public $ihgf_usage_elements_map = array(
      'body'      => 'html body',
      'heading1'  => 'h1',
      'heading2'  => 'h2',
      'heading3'  => 'h3',
      'heading4'  => 'h4',
      'heading5'  => 'h5',
      'heading6'  => 'h6',
      'blockquote'=> 'blockquote',
      'p'         => 'p',
      'li'        => 'li',
      'a'         => 'a',
      'span'      => 'span'
    );
        
    public $ihgf_element_names = array(
      'googlefonts_font1' => 'googlefont1',
      'googlefonts_font2' => 'googlefont2',
      'googlefonts_font3' => 'googlefont3',
    );

    public $font_styles_translation = array(
      '100' => 'Ultra-Light',
      '200' => 'Light',
      '300' => 'Book',
      '400' => 'Normal',
      '500' => 'Medium',
      '600' => 'Semi-Bold',
      '700' => 'Bold',
      '800' => 'Extra-Bold',
      '900' => 'Ultra-Bold',
      'regular' => 'Normal 400'
    );

    public $ihgf_fonts = array();

    // PHP 4 Compaiable construct
    public function Iwebsite_hebrew_google_fonts() { $this->_construct(); }
    
    // PHP Construct function
    public function _construct() {
      $locale = get_locale();
      $mo_file = dirname( __FILE__ ).'/languages/' . $this->localizationDomain . '-'. $locale . '.mo';
      load_textdomain( $this->localizationDomain, $mo_file );
      $this->thispluginurl  = ihgf_PLUGIN_URL;
      
      $this->getOptions();
      $this->ihgf_fonts = get_option($this->ihgf_data_option_name);
      $this->ihgf_usage_elements = $this->ighf_label_to_array();
      // Actions 
      add_action( 'admin_menu', array( $this, 'admin_menu_link' ) );
      add_action( 'admin_enqueue_scripts',array( $this,'ihgf_admin_scripts') );
      add_action( 'wp_enqueue_scripts', array( $this, 'iwebsite_hebrew_fonts_enqueues' ) );
      add_action( 'wp_head', array( $this,'add_hebrew_google_fonts_css') ); 
      add_action( 'wp_ajax_iwebsite_hebrewfont_action', array( $this, 'iwebsite_hebrewfont_action_callback') );
      // Filters
      add_filter( 'tiny_mce_before_init', array( $this, 'ihgf_wp_mce_fonts_array') );
      add_filter( 'mce_buttons_3', array( $this, 'ihgf_wp_mce_buttons' ) );
      add_action( 'init', array( $this, 'ihgf_wp_mce_fonts_styles' ) );
    }

    // add to admin-panel page with settings
    public function admin_menu_link() {
      add_menu_page( 'iWebsite Hebrew Fonts', 'Hebrew Fonts', 'manage_options', 'iwebsite-hebrew-googlefonts', array($this,'admin_options_page') , 'dashicons-editor-textcolor');
    }

    public function ihgf_admin_scripts() {
        wp_enqueue_style('igfh-admin-style',$this->thispluginurl . '/styles/igfh-admin-style.css', array(), '');
        wp_enqueue_script( 'igfh-admin-ajax',$this->thispluginurl . '/scripts/igfh-admin-ajax.js',array('jquery') );
        wp_localize_script( 'igfh-admin-ajax', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    }

    /**
    * Adds settings/options page
    */
    public function admin_options_page() { 
      $message = null;
      if ( isset( $_POST['iwebsite_hebrew_google_fonts_save'] ) && $_POST['iwebsite_hebrew_google_fonts_save'] ) {
          if ( $this->ihgf_handle_submission( $_POST ) ) {
            $message = '<div class="updated"><p>' . __( 'Success! Your changes were sucessfully saved!', $this->localizationDomain ) . '</p></div>';
          } else{
            $message = '<div class="error"><p>' . __( 'Error. Either you did not make any changes, or your changes did not save. Please try again.', $this->localizationDomain ) . '</p></div>';
          }
      }
      ?>                                
      <div class="wrap">
        <table width="650" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <form method="post" id="googlefonts_options">
                <?php wp_nonce_field( 'iwebsite-hebrew-googlefonts-update-options' ); ?>
                <h2><?php _e( 'Select Fonts', $this->localizationDomain );?></h2>
                <?php if ( $message ) { echo $message; } ?>
                <?php 
                  /* This call gets all the font boxes and also sets some of the class options.
                   * ihgf_print_notices must be called after ihgf_get_selection boxes, or the notices
                   * will be empty.
                  */
                  echo $this->ihgf_get_selection_boxes( $this->ihgf_element_names );
                ?>
                <h2><?php _e( 'Troubleshooting', $this->localizationDomain ); ?></h2>
                <p><?php  _e( 'This plugin uses open source fonts that are hosted on Google&rsquo;s servers. For more information about this service, you can visit the', $this->localizationDomain ); ?> 
                  <a href="http://www.google.com/fonts/"><?php _e('Google Font Directory', $this->localizationDomain); ?></a>.
                </p>
                <hr />
              </form>
            </td>
          </tr>
        </table>
      </div>
    <?php
    } //end admin_options_page

  // Get query to enequeue font with all chosen variations ( like: "family=Example:400,500,600" ) 
    public function get_api_query() {
      $query = null;
      $fonts = $this->options['ihgf_selections'];
      $families = array();
      
      if ( $fonts && is_array( $fonts ) ) {
        $i = 0;
        foreach ( $fonts as $fontvars ) {
          if ( isset( $fontvars['family'] ) && $fontvars['family'] ) {
            /* Proper Case everything, otherwise Google does not recognize it */
            $words = explode( "-",$fontvars['family'] );
            foreach ( $words as $key => $word ) {
              $words[$key] = ucwords( $word );
            }
            
            $families[$i] = implode( '+', $words );
            if ( isset( $fontvars['variants']) && !empty($fontvars['variants'] ) ) {
              /* Convert 'regular' and 'italic' to be the same way Google does it.
               * It works without converting it, but we do it for the sake of consistency */
              foreach ( $fontvars['variants'] as $key => $var ) {
                if ( $var == 'regular' ) { $fontvars['variants'][$key] = '400'; }
                if ( $var == 'italic' ) { $fontvars['variants'][$key] = '400italic'; }
              }
               
              $families[$i] = $families[$i] . ":" . implode(",", $fontvars['variants']);
            }
          }
          $i++;
        }
        
        $query .= "?family=" . implode("|", $families);
      }
      return $query;
    }

    // Enequeue fonts, depending on what fonts has been chosen by user
    public function iwebsite_hebrew_fonts_enqueues() {
      $have_selections = false;
      $options = get_option( 'ihgf_options' );
      if ( isset( $options['ihgf_selections'] ) ) {
        $fonts = $options['ihgf_selections'];
      }

      if ( !empty( $fonts ) ) {
        $family = '';
        foreach ( $fonts as $val ) {
          if ( isset( $val['family']) && $val['family'] && $val['family'] != 'off' ) {
            $have_selections = true;
            if ( $family != '' )
              $family .= '|';
            $family .= $val['family'];
          }
        }
      }
      
      if ( $have_selections ) {
        // check to see if site is uses https
        $http = ( !empty( $_SERVER['HTTPS'] ) ) ? "https" : "http";
        $url = $http. '://fonts.googleapis.com';
        
        if ( $family == 'Open-Sans-Hebrew' ){
          $url .= '/earlyaccess/opensanshebrew.css';
        } elseif ( $family == 'Alef-Hebrew' ) {
          $url .= '/earlyaccess/alefhebrew.css';
        } else {
          $url .= '/css'.$this->get_api_query();
        }
        
        //possibly add a checkbox to admin to add this code to the head manually if enqueue does not work
        // add_filter( 'style_loader_tag',array( $this,'ihgf_url_filter' ),1000,2 );
        wp_enqueue_style( 'iwebsite_hebrew_googlefonts', $url );
        // remove_filter( 'style_loader_tag',array( $this,'ihgf_url_filter' ) );
      }
    }
    public function ihgf_url_filter( $tag=null, $handle=null ) {
      if ( $handle == 'iwebsite_hebrew_googlefonts' ) {
        //put back in regular ampersands //
        $tag = str_replace( '#038;', '', $tag );
      }
      return $tag;
    }

    // Output css font style in the head
    public function add_hebrew_google_fonts_css() {
      $uses = $this->ihgf_usage_elements_map;
      $names = $this->ihgf_element_names;
      $styles = null;
    
      /* do for all in ihgf_element_names */
      if ( $uses && $names ) {
        foreach ( $names as $font => $name ) {
          $family = null;
          if ( isset( $this->options['ihgf_selections'][$name]['family'] ) ) {
            $family = $this->options['ihgf_selections'][$name]['family'];
          }
          if ( $family && $family != 'off' ) {
            foreach ( $uses as $key => $tag ) {
              if ( $this->options[$name . "_" . $key] == 'checked' ) {
                $styles .= "\t" . $tag . '{ font-family:"'.str_replace( "-", " ", $family ).'", arial, sans-serif!important;}';
                $styles .= "\n";
              }
            }
            
            if ( trim( $this->options[$name . '_css'] ) ) {
              $styles .= "\t" . trim( stripslashes($this->options[$name . '_css']) ). "\n";
            }
          }
        }
      }
      
      if ( $styles ) {
        echo "<style type='text/css' media='screen'>\n";
        echo $styles;
        echo "</style>\n";
      }
    }

    public function ihgf_get_selection_boxes( $element_names ) {
      $this->ihgf_get_font_file();
      $out = null;
      if ( is_array( $element_names ) ) {
        $i = 1;
        foreach ( $element_names as $name ) {
          $out.= $this->ihgf_get_fonts_select( $name, $i );
          $i++;
        }
      }
      return $out ;
    }

    /* output select with fonts list*/
    public function ihgf_get_fonts_select( $name = "googlefont", $i ) {     
      //prefill all the select boxes because there's not as much overhead there.
      $out = null;
      $next_i = $i+1;
      if ( $this->ihgf_fonts && is_array( $this->ihgf_fonts ) ) {
        $variants= null;
        $first = true;
        
        $current_selection = $this->options['ihgf_selections'][$name]['family'];
        if ( empty($current_selection) ){
          $current_selection = 'off';
        }
        if ( $current_selection == "off" && $i != 1 ){
          $class = "hide";
        } else {
          $class = "";
        }
        $out .= "<div id='".$name."' class='googlefont-block ".$class."'>\n";
        $out .="<h2>" . sprintf( __( 'Font %s', $this->localizationDomain ), $i ) . "</h2>\n";  
        $out .= "<select name='".$name."[family]' id='".$name."-select' class='webfonts-select'>\n";
        
        foreach ( $this->ihgf_fonts as $font ) {
          $class = array();     
          $has_variants = false;
          $normalized_name = $this->ihgf_normalize_font_name( $font->family );
          $is_selection = false;
          
          if ( $normalized_name == $current_selection ) {
            $is_selection = true;
          }
          
          $class[] = $normalized_name;
          
          if ( count( $font->variants ) > 1 ) {
            $class[] = "has-variant";
          }

          /* write the blank and 'none options on first pass */
          if ( $first ) {
            $first = false;
            $out.="<option class='".implode( " ", $class )."' value='off' ".
                $this->ihgf_is_selected( $normalized_name, $current_selection ).
                ">" . __( "None (Turn off this font)", $this->localizationDomain ) . "</option>\n";
          }
          
          /* write the option */
          $out.="<option class='".implode( " ", $class )."' value='".$normalized_name.
              "' ".$this->ihgf_is_selected( $normalized_name, $current_selection ).">" . $font->family . "</option>\n";
          
          if ( $is_selection ) {
            /* get the font family variants */
            $variants = $this->ihgf_get_font_variants( $name, $font, $is_selection );
          }
        }
        
        $out.= "</select>\n";
        
        //if a font is already selected, get all of its details
        //otherwise, create a blank input for each.
        if ( !$variants ) {
          $variants = '<input type="checkbox" name="'.$name.'[variants][]" value="regular" class="check '.$normalized_name.' blank"> <label>Normal 400</label>';
        }
        
        if ( $current_selection && $current_selection != 'off' ) {
          $out .= '<a href="#'.$name.'-select" class="show-hide button" id="show-hide-'.$name.'"><span class="show-label">' . __( 'Show Options', $this->localizationDomain ) . '</span><span class="hide-label">' . __( 'Hide Options', $this->localizationDomain ) . '</span></a>';
        }
        
        /* add an ajax message div to indicate loading/waiting message or image */
        $out.='<div class="webfonts-waiting"><div class="igfh-spinner"></div></div>';
        if ( $current_selection && $current_selection != 'off' ) {
        /* add a section for additional selections */
          $out.= $this->ihgf_get_font_selections( $name, $variants );
        }
        $out.='<div style="" class="ighf-save-button"><input class="button-primary" type="submit" name="iwebsite_hebrew_google_fonts_save" value="' . __( 'Save All Fonts', $this->localizationDomain ) . '" /></div>';
        
        if ( $i != 3 ){
            $out.= '<a href="#" title="" class="special-element-font" data-font-id="'.$next_i.'">'.__('Do you want choose font for special element?', $this->localizationDomain ).'</a>';
        }
        
        $out.="<hr>";

        $out.="</div>\n";
      }
      
      return $out;
    }

    public function ihgf_get_font_file() {
      $fonts = null;
      $fonts_object = null;
      
      $this->ihgf_check_font();

      $json = get_option( $this->ihgf_data_option_name );

      if ( $json ) {
        $fonts_object = json_decode( $json );
      }

      if ( $fonts_object && is_object( $fonts_object ) ) {
        if ( $fonts_object->items && is_array( $fonts_object->items ) ) {
          $fonts = $fonts_object->items;
        }
      }
      $this->ihgf_fonts = $fonts;
    }


    public function ihgf_check_font() {
      $updated = false;
      $fonts_json = NULL;
      
      /* get fonts list from local file, */

      if ( !$this->ihgf_fonts ) {
        $fonts_json = $this->ihgf_get_fonts_list();
      }

      if ( $fonts_json ) {
        /* put into option in WordPress */
        $updated = update_option( $this->ihgf_data_option_name, $fonts_json );
      }

      return $updated;
    }

    public function ihgf_get_fonts_list() {
      $fonts = null;
      
      include $this->ihgf_fonts_file;

      if ( $fonts ) {
        $this->ihgf_notices[] = __("Using the local font list file because we could not connect with Google.", $this->localizationDomain );
      } else {
        $this->ihgf_notices[] = __("Local font list file cannot be found or does not exist.", $this->localizationDomain );
      }
    
      return $fonts;
    }

    // Output notices
    public function ihgf_print_notices() {    
      $out = null;
      if ( $this->ihgf_notices ) {
        $out.= "<ul class='notices'>\n";
        foreach ( $this->ihgf_notices as $notice ) {
          $out.= "<li class='notice'>".$notice."</li>\n";
        }
        $out.= "</ul>\n";
      }
      return $out;
    }

    public function ihgf_normalize_font_name( $name ) {
      return( str_replace( " ", "-", trim( $name ) ) );
    }

    // Detect which option selected
    public function ihgf_is_selected( $item, $compare ) {
      if ( is_string( $item ) ) { $item = strtolower( $item ); }
      if ( is_string( $compare ) ) { $compare = strtolower( $compare ); }
      
      if ( $item == $compare ) {
        return ( 'selected=selected' );
      }
      return null;
    }
  

    // Detect which variant has been chosen
    public function ihgf_is_variant_checked( $item, $compare ) {
      $checked = ' checked=checked';
      if ( is_string( $item ) ) { $item = strtolower( $item ); }
      if ( is_string( $compare ) ) { $compare = strtolower( $compare ); }
      
      if ( is_array( $compare ) && $compare ) {
        if ( in_array( $item, $compare ) ) {
          return $checked;
        }
      } elseif ( $item == $compare ) {
        return $checked;
      }
      return null;
    }

    // Output selection in admin panel
    public function ihgf_get_font_selections( $name, $variants ) {
      $out = null;
      $out.= "<div class='webfonts-selections hide'>\n";

      /* add in all variants that will appear through jQuery */
      $out.= "<div class='webfonts-variants'><h3>" . __('1. Choose the font styles you want:*', $this->localizationDomain) . "</h3>\n".$variants."</div>\n";
      
      /* add in the dom elements the user would like it to affect and custom css box */
      $out.= "<div class='webfonts-usage'><h3>" . __('2. Elements you want to assign this font to:*', $this->localizationDomain) . "</h3>\n".$this->ihgf_get_usage_checkboxes($name)."</div>\n";
      $out.="</div>";
      return $out;
    }

    public function ihgf_get_font_data_by_family( $googlefont, $family, $data_type ) {
      $data = null;
      if ( is_string($family) ) {
        $font = null;
        
        if ( $this->ihgf_fonts ) {
          if ( !is_array( $this->ihgf_fonts ) ) {
            $fonts = json_decode( $this->ihgf_fonts );
          }else{
            $fonts = $this->ihgf_fonts;
          }

          foreach ( $fonts->items as $findfont ) {
            if ( $this->ihgf_normalize_font_name( $findfont->family ) == $family ) {
              $font = $findfont;
            }
          }
        }
        if ( $font && is_object( $font ) ) {
          if ( $data_type == 'variants' ) {
            $data = $this->ihgf_get_font_variants( $googlefont, $font );
          }
        }
      }
      
      return $data;
    }

    // Get all avaible checkboxes in admin panel
    public function ihgf_get_usage_checkboxes( $element ) {
      $out = null;
      if ( is_array( $this->ihgf_usage_elements ) ) {
        /* get current selections */
        foreach ( $this->ihgf_usage_elements as $key => $val ) {
          $checked = null;
          
          if ( $this->options[$element."_".$key] == "checked" ) {
            $checked = ' checked="checked"';
          }

          $out .= '<input type="checkbox" id="' . $element . "_" . $key . '" name="'.
              $element.'[usage][]" value="'.$key.'"'.$checked.'> <label for="' . 
              $element . "_" . $key . '">'.$val.'</label><br>';
        }
      }
      return $out;
    }

    // Get avaible font variants ( boldness etc.)
    public function ihgf_get_font_variants( $name, $font=null, $is_selection=false ) {
      $variants = null;
      if ( $font && isset( $font->variants ) ) {
        $normalized_name = $this->ihgf_normalize_font_name( $font->family );
        $has_variants = false;
        if ( count( $font->variants ) > 1 ) {
          $has_variants = true;
        }

        ksort( $font->variants );
        $variants .= "<div class='variant-".$normalized_name." variant-items'>\n";
        $vid = null;
        foreach ( $font->variants as $variant ) {
          $vid = $this->ihgf_normalize_font_name( $font->family . " " . $this->ihgf_fancy_font_name( $variant ) );
          $vchecked = null;
          $readonly = null;
          if ($is_selection) {
            $vchecked = $this->ihgf_is_variant_checked( $variant, $this->options['ihgf_selections'][$name]['variants'] );
          }
          if ( $is_selection && !$has_variants ) {
            $readonly = " readonly='readonly'";
          }
          $variants .= '<input type="checkbox" id="'.$vid.'" name="'.$name.'[variants][]" value="'.
                  $variant.'" class="check ' . $normalized_name.'"'. $vchecked . $readonly . '> <label for="'.$vid.
                  '">' . $this->ihgf_fancy_font_name( $variant ) . '</label><br>';
          
        }
        $variants .= "</div>\n";
      }
      return $variants;
    }

    public function ihgf_fancy_font_name( $name ) {
      $ids = $this->font_styles_translation;
      $text = array();
      
      foreach ( $ids as $key=>$val ) {
        $pos = stripos( (string)$name, (string)$key );
        if ( $pos !== false ) {
          if ( $key == 'regular' ) {
            $key = null;
          }
          $text[]= "$val $key";
        }
      }
      
      if ( stripos( $name,'italic' ) !== false ) {
        $text[]='Italic';
      }
      
      if ( $name == 'italic' ) {
        $text = array( 'Normal 400 Italic' );
      }
      
      $name = implode( ' ',$text );
      return $name;
    }

    /**
    * Retrieves the plugin options from the database.
    * @return array
    */
    public function getOptions() {
      //Set up the default options
      if ( !$theOptions = get_option( $this->optionsName ) ) {
        $theOptions = array(
          /* leave for backwards compatability */
          'googlefonts_font1'=>'',
          'googlefonts_font2'=>'',
          'googlefonts_font3'=>'',       
          'googlefont1_css'=>' ',
          'googlefont1_heading1'=>'checked',
          'googlefont1_heading2'=>'checked',
          'googlefont1_heading3'=>'checked',
          'googlefont1_heading4'=>'checked',
          'googlefont1_heading5'=>'checked',
          'googlefont1_heading6'=>'checked',
          'googlefont1_body'=>'checked',
          'googlefont1_blockquote'=>'checked',
          'googlefont1_p'=>'checked',
          'googlefont1_li'=>'checked',
          'googlefont1_a'=>'checked',
          'googlefont1_span'=>'checked',


          'googlefont2_css'=>' ',
          'googlefont2_heading1'=>'checked',
          'googlefont2_heading2'=>'checked',
          'googlefont2_heading3'=>'checked',
          'googlefont2_heading4'=>'checked',
          'googlefont2_heading5'=>'checked',
          'googlefont2_heading6'=>'checked',
          'googlefont2_body'=>'checked',
          'googlefont2_blockquote'=>'checked',
          'googlefont2_p'=>'checked',
          'googlefont2_li'=>'checked',
          'googlefont2_a'=>'checked',
          'googlefont2_span'=>'checked',            

          'googlefont3_css'=>' ',
          'googlefont3_heading1'=>'checked',
          'googlefont3_heading2'=>'checked',
          'googlefont3_heading3'=>'checked',
          'googlefont3_heading4'=>'checked',
          'googlefont3_heading5'=>'checked',
          'googlefont3_heading6'=>'checked',
          'googlefont3_body'=>'checked',
          'googlefont3_blockquote'=>'checked',
          'googlefont3_p'=>'checked',
          'googlefont3_li'=>'checked',
          'googlefont3_a'=>'checked',
          'googlefont3_span'=>'checked',
          
          'googlefont_data_time' => 0,
          'ihgf_selections' => array(
              'googlefont1' => array(
                'family' => null,
                'variants' => array('regular', '700'),
              )
            ),
        );
        update_option( $this->optionsName, $theOptions );

      }
      $this->options = $theOptions;
            
    }

    // use for saving user settings
    public function ihgf_handle_submission( $data ) {
      if ( !wp_verify_nonce( $_POST['_wpnonce'], 'iwebsite-hebrew-googlefonts-update-options' ) ) 
        die( __('Whoops! There was a problem with the data you posted. Please go back and try again.', $this->localizationDomain) ); 
      
      if ( is_array( $data ) ) {
        foreach ( $data as $googlefont => $options ) {
          
          if ( is_array( $options ) && in_array( $googlefont, $this->ihgf_element_names ) && $googlefont['family'] != 'off' ) {
            /* store the family, variants, css and usage options */
            foreach ( $options as $option => $value ) {
              if ( $option=='family' || $option=='variants' ) {
                $this->options['ihgf_selections'][$googlefont][$option] = $value;
              }
            }
            //have to check and set all usage options separately because they are not an array
            if ( isset( $options['usage'] ) && is_array( $options['usage'] ) && $options['usage'] ) {
              foreach ( $this->ihgf_usage_elements as $key => $val ) {
                if ( in_array( $key, $options['usage'] ) ) {
                  $this->options[$googlefont . "_" . $key] = 'checked';
                }else{
                  $this->options[$googlefont . "_" . $key] = 'unchecked';
                }
              }
            } else {
              foreach ( $this->ihgf_usage_elements as $key => $val ) {
                $this->options[$googlefont . "_" . $key] = 'checked';
              }
            }
          }
        }
        return ( $this->saveAdminOptions() );
      }
      return false;      
    }

    public function saveAdminOptions() {
        return update_option( $this->optionsName, $this->options );
    }

    // ajax handling, for bringing avaible variants
    public function iwebsite_hebrewfont_action_callback() {
      $name = sanitize_text_field( $_POST['googlefont_ajax_name'] );
      $family = sanitize_text_field( $_POST['googlefont_ajax_family'] );
      $normalized_name = $this->ihgf_normalize_font_name( $family );

      $variants = $this->ihgf_get_font_data_by_family( $name, $family, 'variants' );
      
      if (!$variants) {
        $variants = '<input type="checkbox" name="'.$name.'[variants][]" value="regular" class="check '.$normalized_name.' blank"> <label>Normal 400</label>';
      }
      echo $this->ihgf_get_font_selections( $name, $variants );
      die();
    }

    // Add custom fonts for use with the visual editor
    public function ihgf_wp_mce_fonts_styles() {
      $font_url = 'http://fonts.googleapis.com/earlyaccess/alefhebrew.css';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'https://fonts.googleapis.com/css?family=Amatica+SC';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'https://fonts.googleapis.com/css?family=Assistant';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'https://fonts.googleapis.com/css?family=Arimo';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'https://fonts.googleapis.com/css?family=Cousine';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'https://fonts.googleapis.com/css?family=David+Libre';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'https://fonts.googleapis.com/css?family=Frank+Ruhl+Libre';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'https://fonts.googleapis.com/css?family=Heebo';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'https://fonts.googleapis.com/css?family=Miriam+Libre';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'http://fonts.googleapis.com/earlyaccess/notosanshebrew.css';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'http://fonts.googleapis.com/earlyaccess/opensanshebrewcondensed.css';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'https://fonts.googleapis.com/css?family=Rubik';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'https://fonts.googleapis.com/css?family=Suez+One';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = 'https://fonts.googleapis.com/css?family=Secular+One';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );  
      $font_url = 'https://fonts.googleapis.com/css?family=Tinos';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );  
      $font_url = 'https://fonts.googleapis.com/css?family=Varela+Round';
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = plugins_url( '/fonts/miriamclm/stylesheet.css', __FILE__ );
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = plugins_url( '/fonts/comixno2/stylesheet.css', __FILE__ );
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
      $font_url = plugins_url( '/fonts/nehama/stylesheet.css', __FILE__ );
      add_editor_style( str_replace( ',', '%2C', $font_url ) );
    }

    // Enable font family drop down in the visual editor
    function ihgf_wp_mce_buttons( $buttons ) {
        array_unshift( $buttons, 'fontselect' );
        return $buttons;
    }

    // Add Hebrew fonts to the Fonts list
    function ihgf_wp_mce_fonts_array( $initArray ) {
        $initArray['font_formats'] = 
          'Alef=Alef Hebrew;' .                
          'Andale Mono=andale mono,times;' .
          'Arial=arial,helvetica,sans-serif;' .
          'Arial Black=arial black,avant garde;' .
          'Arimo=arimo,sans-serif;' .
          'Assistant=assistant,sans-serif;' .
          'Amatica=amatica,cursive;' .

          'David Libre=David Libre,serif;' .
          'Miriam Libre=miriam libre,sans-serif;' .
          'Frank Ruhl Libre=frank ruhl libre,sans-serif;' .
          'Suez One=suez one,serif;' .
          'Secular One=secular one,sans-serif;' .
          'Heebo=heebo,sans-serif;' .
          'Cousine=cousine,monospace;' .
          'Varela Round=varela round,sans-serif;' .
          'Rubik=rubik,sans-serif;' .
          'Tinos=cousine,serif;' .

          'Book Antiqua=book antiqua,palatino;' .
          'Comic Sans MS=comic sans ms,sans-serif;' .
          'Comix No 2=comix_no2_clm;' .
          'Courier New=courier new,courier;' .
          'Georgia=georgia,palatino;' .
          'Helvetica=helvetica;' .
          'Impact=impact,chicago;' .
          'Lato=Lato;' .
          'Miriam=miriam_clm;' .       
          'Nehama=nehamaregular;' . 
          'Noto Sans Hebrew=Noto Sans Hebrew;' .                
          'Open Sans Hebrew Condensed=Open Sans Hebrew Condensed;' .                
          'Tahoma=tahoma,arial,helvetica,sans-serif;' .
          'Terminal=terminal,monaco;' .
          'Times New Roman=times new roman,times;' .
          'Trebuchet MS=trebuchet ms,geneva;' .
          'Verdana=verdana,geneva;' .
          'Webdings=webdings;' .
          'Wingdings=wingdings,zapf dingbats';
        return $initArray;
    }

    public function ighf_label_to_array(){
      $ar_usage_elements = array(
        'body'       => __( "Body tag", $this->localizationDomain), 
        'heading1'   => __( 'Headline 1 (h1 tags)', $this->localizationDomain), 
        'heading2'   => __( 'Headline 2 (h2 tags)', $this->localizationDomain), 
        'heading3'   => __( 'Headline 3 (h3 tags)', $this->localizationDomain), 
        'heading4'   => __( 'Headline 4 (h4 tags)', $this->localizationDomain), 
        'heading5'   => __( 'Headline 5 (h5 tags)', $this->localizationDomain), 
        'heading6'   => __( 'Headline 6 (h6 tags)', $this->localizationDomain), 
        'blockquote' => __( 'Blockquotes', $this->localizationDomain), 
        'p'          => __( 'Paragraphs (p tags)', $this->localizationDomain), 
        'li'         => __( 'Lists (li tags)', $this->localizationDomain), 
        'a'          => __( 'Links', $this->localizationDomain), 
        'span'       => __( 'Span', $this->localizationDomain)
      );
      return $ar_usage_elements;
    }

  }
}

//instantiate the class
if ( class_exists( 'Iwebsite_hebrew_google_fonts' ) ) {
    $googlefonts_var = new Iwebsite_hebrew_google_fonts();
}