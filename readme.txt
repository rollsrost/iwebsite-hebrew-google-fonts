=== WP Google Fonts ===
Contributors: iWebsite
Tags: Google fonts, fonts, font, free fonts, Google hebrew fonts, hebrew fonts, hebrew font, typography, css, design, plugin, WordPress Google Fonts Plugin, Google Webfonts, Google Fonts WordPress, WordPress Fonts, Theme Fonts, Theme Fonts Plugin
Requires at least: 4.6.1
Tested up to: 4.6.1
Stable tag: trunk
License: GPLv2 or later

The WP Google Fonts plugin allows you to easily add fonts from the Google Font Directory to your Wordpress theme. 

== Description ==
The one of the most useful and great developments in web typography - is the Google's free font directory. The amazing variety and quality of this free font resource has made him the most popular font plugin for Wordpress sites and it popularity grow without stopping.

Our plugin is very easy and simple way to add custom google fonts to any WordPress powered site in hebrew language without coding. It even easier to use Google's free service to add high quality fonts to your Wordpress theme. Not only does this plugin add the necessary Google code, but it also gives you the ability to assign the Google fonts to specific CSS selectors of your website from within the Wordpress in the admin settings area. Or if you would rather, you can target the Google fonts from your own theme's stylesheet. It's compatible with any theme. 

= What does this plugin do? =

* This plugin allows you to **take full control of your theme's typography** in any WordPress theme (no coding required).
* It allows you to **choose from 14 hebrew google fonts** and font variants to insert into your website without coding.
* Allows you to **create your own font controls and rules** in the admin area (no coding required).
* Allows you to easily change the look of your website with the click of a button.
* **Automatically enqueues all stylesheets for your chosen google fonts**.
* Allows you to add google fonts to your theme without editing the themes main stylesheet which allows you to update your theme without losing your custom google fonts.

= Who is this Plugin ideal for? =
* Anyone who is looking for an easy way to use google fonts in their theme without coding.
* Theme Authors: you can use this plugin to add custom google webfonts to your theme.
* Great for use on client projects or for use on existing websites.
* People that are happy with their theme but want an easy way to change the typography.
* Anyone with basic knowledge of CSS Selectors (in order to add custom font rules).